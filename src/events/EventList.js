import React, { useState, useEffect } from "react";
import axios from "axios";

import EventItem from "./EventItem";

// This component will fetch the data from API and render EventItems for each received event
const EventList = function({ provider }) {
  const [events, setEvents] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(async () => {
    try {
      const { data, status } = await axios.get(
        `https://api.fitogram.pro/providers/${provider}/events/public?from=${new Date().toISOString()}`
      );
      if (status === 200) {
        setEvents(data);
      } else {
        throw new Error(`API call failed with status ${status}`);
      }
    } catch (err) {
      console.error(err);
    } finally {
      setIsLoading(false);
    }

    return () => {
        console.log("test")
    }
  }, []);

  return (
    <div className="events">
      <hr className="top" />

      {isLoading ? (
        <p>Loading...</p>
      ) : (
        events.map(event => (
          <div key={event.id}>
            <EventItem {...event} />
            <hr className="between" />
          </div>
        ))
      )}
    </div>
  );
};

export default EventList;
