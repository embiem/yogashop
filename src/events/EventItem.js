import React, { useState } from "react";
import { format } from "date-fns";
import PlaceholderImg from "./placeholder.jpg";
import ClockIcon from "./clock-regular.svg";
import MapMarkerIcon from "./map-marker.svg";
import strip from "../utils/stripHTML";

import "./Events.css";

const DESCRIPTION_MAX_LENGTH = 100;

const Button = function({ children, ...rest }) {
  return <button {...rest}>{children}</button>;
};

const Row = function({ children, ...rest }) {
  return (
    <div {...rest} className="row">
      {children}
    </div>
  );
};

const EventDescription = function({ descriptionHTML }) {
  // To avoid dangerously setting html & showing styling in here,
  // we strip the HTML tags
  const cleanDescription = strip(descriptionHTML);

  const [showMore, setShowMore] = useState(false);

  if (cleanDescription.length > DESCRIPTION_MAX_LENGTH && !showMore) {
    return (
      <p className="description">
        {cleanDescription.substr(0, DESCRIPTION_MAX_LENGTH)}{" "}
        <span className="more" onClick={() => setShowMore(true)}>
          ... more
        </span>
      </p>
    );
  }

  return (
    <p className="description">
      {cleanDescription}{" "}
      {cleanDescription.length > DESCRIPTION_MAX_LENGTH && (
        <span className="more" onClick={() => setShowMore(false)}>
          ... less
        </span>
      )}
    </p>
  );
};

const EventName = function({ imageUrl, name, color }) {
  return (
    <div
      className="name"
      style={{
        backgroundImage: `url(${imageUrl || PlaceholderImg})`,
        border: `1px solid ${color}`
      }}
    >
      <h2>{name}</h2>
    </div>
  );
};

const EventInfo = function({
  startDate,
  endDate,
  location,
  trainer,
  description
}) {
  startDate = new Date(startDate);
  endDate = new Date(endDate);

  trainer = trainer || { name: "Unknown" };

  return (
    <div className="info">
      <Row>
        <Row>
          <div>
            <img width={16} src={ClockIcon} alt="clock icon" />
          </div>
          <div style={{ width: 12 }} />
          <div className="date">
            <div>{format(startDate, "dd[.] DD MMM[.] YYYY")}</div>
            <div>
              {format(startDate, "HH[:]mm")} - {format(endDate, "HH[:]mm")}
            </div>
          </div>
        </Row>

        <div className />
      </Row>

      <Row>
        <Row>
          <div>
            <img width={16} src={MapMarkerIcon} alt="map marker icon" />
          </div>
          <div style={{ width: 12 }} />
          <div className="location">{location.name}</div>
        </Row>

        <Row>
          {trainer.imageUrl ? (
            <img className="avatar" alt={trainer.name} src={trainer.imageUrl} />
          ) : (
            <div style={{ display: "inline-block" }} className="avatar">
              {trainer.name.split(" ").map(part => part[0].toUpperCase())}
            </div>
          )}
          <div style={{ width: 12 }} />
          <div>{trainer.name}</div>
        </Row>
      </Row>
      <Row>
        <EventDescription descriptionHTML={description} />
      </Row>
    </div>
  );
};

const EventBooking = function({ seats, seatsBooked }) {
  return (
    <div className="booking">
      <div className="seats">
        {seats - seatsBooked} / {seats} verfügbar
      </div>
      <Button>Zur Buchung</Button>
    </div>
  );
};

const EventItem = function({
  name,
  startDateTime,
  endDateTime,
  seats,
  seatsBooked,
  color,
  eventGroup: { name: groupName, imageUrl },
  location,
  trainers,
  descriptions
}) {
  return (
    <div className="item">
      <EventName imageUrl={imageUrl} name={name || groupName} color={color} />
      <div className="spacer" />
      <EventInfo
        startDate={startDateTime}
        endDate={endDateTime}
        location={location}
        trainer={trainers.length > 0 ? trainers[0] : null}
        description={descriptions.length > 0 ? descriptions[0].text : "<p></p>"}
      />
      <div className="spacer" />
      <EventBooking seats={seats} seatsBooked={seatsBooked} />
    </div>
  );
};

export default EventItem;
