import React, { Component } from "react";
import "./App.css";

import EventList from "./events/EventList";

class App extends Component {
  render() {
    return (
      <div className="App">
        <EventList provider="yogashop" />
      </div>
    );
  }
}

export default App;
